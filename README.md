#ZP - Mail

***This is currently a thought-experiment. Nothing more.***

**Tools**:

EmberJS

Uses PouchDB (clientside) and CouchDB (serverside/iriscouch)

Some sort of two-way encryption via JS library. Preferably Whirlpool and AES combination.

**Purpose**: Store and send mail...

**Features**:

* Can send to other addresses.
* Can recieve from other addresses.
* Can store contact lists.
* Can subscribe to broadcasts.
* Can become broadcast-only addresses.
* ... Can send to normal email addresses?? (Via some sort of centralised IMAP server)
* ... Can recieve from normal email addresses?? (Via some sort of centralised IMAP server)
